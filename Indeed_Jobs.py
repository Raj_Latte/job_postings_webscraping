#!/usr/bin/env python
# coding: utf-8

# # Indeed WebScraping

# In[1]:


import csv
from datetime import datetime
import requests
from bs4 import BeautifulSoup
from datetime import time


# In[2]:


templete = 'https://www.indeed.com/jobs?q={}&l={}'


# In[3]:


def get_url(position, location):
    '''Generate a utl from posotion and location'''
    templete = 'https://www.indeed.com/jobs?q={}&l={}'
    url = templete.format(position, location)
    return url


# In[4]:


url = get_url('data scientist', 'portland or')


# In[5]:


response = requests.get(url)


# In[6]:


response


# In[7]:


response.reason


# In[8]:


soup = BeautifulSoup(response.text, 'html.parser')


# In[9]:


cards = soup.find_all('div', 'jobsearch-SerpJobCard')


# In[10]:


len(cards)


# ### Prototyping the model with single record

# In[11]:


card = cards[2]


# In[12]:


atag = card.h2.a


# In[13]:


job_title = atag.get('title')


# In[14]:


job_url = 'https://www.indeed.com/' + atag.get('href')


# In[15]:


company = card.find('span', 'company').text.strip()


# In[16]:


job_location = card.find('div', 'recJobLoc').get('data-rc-loc')


# In[17]:


job_summary = card.find('div', 'summary').text.strip()


# In[18]:


post_date = card.find('span', 'date').text


# In[19]:


today = datetime.today().strftime('%Y-%m-%d')


# In[20]:


try:
    job_salary = card.find('span', 'salaryText').text.strip()
except AttributeError:
    job_salary = ''


# ### Generalize a model with a function.

# In[21]:


def get_record(card):
    '''Extract the job data from single record'''
    atag = card.h2.a
    job_title = atag.get('title')
    job_url = 'https://www.indeed.com/' + atag.get('href')
    company = card.find('span', 'company').text.strip()
    job_location = card.find('div', 'recJobLoc').get('data-rc-loc')
    job_summary = card.find('div', 'summary').text.strip()
    post_date = card.find('span', 'date').text
    today = datetime.today().strftime('%Y-%m-%d')
    try:
        job_salary = card.find('span', 'salaryText').text.strip()
    except AttributeError:
        job_salary = ''
    
    record = (job_title, company, job_location, post_date, today, job_summary,job_salary, job_url)
    return record


# In[22]:


records = []

for card in cards:
    record = get_record(card)
    records.append(record)


# In[23]:


records[2]


# #### Getting the NEXT Button

# In[24]:


while True:
    try:
        url = 'https://www.indeed.com/' + soup.find('a', {'aria-label':'Next'}).get('href')
    except AttributeError:
        break

    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    cards = soup.find_all('div', 'jobsearch-SerpJobCard')

    for card in cards:
        record = get_record(card)
        records.append(record)


# In[25]:


len(records)


# ### Putting all together

# In[26]:


import csv
from datetime import datetime
import requests
from bs4 import BeautifulSoup

def get_url(position, location):
    '''Generate a utl from posotion and location'''
    templete = 'https://www.indeed.com/jobs?q={}&l={}'
    url = templete.format(position, location)
    return url

def get_record(card):
    '''Extract the job data from single record'''
    atag = card.h2.a
    job_title = atag.get('title')
    job_url = 'https://www.indeed.com/' + atag.get('href')
    company = card.find('span', 'company').text.strip()
    job_location = card.find('div', 'recJobLoc').get('data-rc-loc')
    job_summary = card.find('div', 'summary').text.strip()
    post_date = card.find('span', 'date').text
    today = datetime.today().strftime('%Y-%m-%d')
    try:
        job_salary = card.find('span', 'salaryText').text.strip()
    except AttributeError:
        job_salary = ''
    
    record = (job_title, company, job_location, post_date, today, job_summary,job_salary, job_url)
    return record

def main(position, location):
    '''Run the main program routine'''
    records = []
    url = get_url(position, location)

    #Extract the job data
    while True:
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        cards = soup.find_all('div', 'jobsearch-SerpJobCard')

        for card in cards:
            record = get_record(card)
            records.append(record)

        try:
            url = 'https://www.indeed.com/' + soup.find('a', {'aria-label':'Next'}).get('href')
        except AttributeError:
            break

# save the job data
with open('Indeed.csv', 'w', newline='', encoding='utf-8') as f:
    writer = csv.writer(f)
    writer.writerow(['JobTitle', 'Company', 'Location', 'PostingDate', 'ExtractDate', 'Summary', 'Salary', 'JobUrl'])
    writer.writerows(records)


# In[27]:


#Run the main program
main('Data analyst', 'portland or')

